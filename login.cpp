#include "login.h"
#include <QDir>
#include <bits/stdc++.h>

QString Login::operator [] (QString s)
{
        if(!data.count(s))
        {
                fprintf(stderr,"LOGIN INFORMATION %s NOT FOUND IN %s INSERT IT:\e[107m\n",s.toStdString().c_str(),file.toStdString().c_str());
                char in [12345];
                scanf("%[^\n]",in);
                fprintf(stderr,"\e[49m\n");
                data[s]=in;

                exit(0);
        }
        return data[s];
}
Login::Login(QString file_in):file(file_in)
{
        QFile f(file);
        if(!f.open(QFile::ReadOnly))
        {
                fprintf(stderr,"LOGIN FILE %s NOT FOUND\n",file.toStdString().c_str());
                //exit(0);
        }
        else
        {
            auto x = QString::fromUtf8(f.readAll()).split("\n");
            for(auto it:x)
            {
                auto a = it.split(' ');
                auto b = a[0];
                a.pop_front();
                data[b] = a.join(' ');
            }
            f.close();
        }

}
Login::Login():Login(QDir::homePath()+"/.config/submiterLogin")
{

}

Login loginData;

#include "colors.h"
QString color::proces(int i)
{
        if(i==-1) return "\e[95";
        i = i%10;
        return "\e["+QString::number(i<5?32+i:92+i-5)+"m";

}
QString color::replaceOkayError(QString in,QString def)
{
        return in.replace("<p class='okay'>",green).replace("<p class='error'>",red).replace("</p>",def);
}

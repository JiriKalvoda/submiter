#define DOUT if(0)
#define DASK if(0)
#define DARG if(1)

#ifndef SUBMITER_H
#define SUBMITER_H

#include <QTextStream>
#include <QVector>

#include "colors.h"
#include "login.h"


extern QTextStream out;


QString rmMulSpace(QString in);
QString ignoreTag(QString in);
using Parameters = QVector<QPair<QString,QString>>;
struct FileParametersItem
{
    QString name;
    QByteArray in;
    QString filename;
    FileParametersItem(QString NAME,QByteArray IN,QString FILENAME):name(NAME),in(IN),filename(FILENAME){}
    FileParametersItem(QString NAME,QByteArray IN):name(NAME),in(IN),filename("file.txt"){}
    FileParametersItem(){}
};
using FileParameters = QVector<FileParametersItem>;

QString wget(QString url,Parameters get={},Parameters post={},FileParameters files={});

struct Submiter
{
    bool isLogIn=0;
    virtual QByteArray exec(QString name,QByteArray in);
    virtual QString makeExec(QString in);
     QMap<QString,QString> par;
    virtual QString detectTask(QString file);
    virtual QString detectType(QString fileName);
    virtual void logIn();
    virtual void parseData(QString ansfer);
    virtual void submit(QString task, QString subtask, QString type, QByteArray data);
    virtual void submitAll(QString task, QString subtask, QString type, QByteArray data);
    virtual int countOfSubtask(QString task);
    virtual void generate(QString task, QString subtask);
    virtual QByteArray download(QString task, QString subtask);
    virtual QByteArray generateDownloadAll(QString task, QString subtask);
    virtual void logOut();
    virtual void run(QString fileName,QMap<QString,QString> parM);
    virtual void noFileRun(QString fileName,QMap<QString,QString> parM);
    virtual ~Submiter();
    virtual QString help();
};


extern QMap<QString, Submiter *> submiter;

extern
QString globalHelp();

struct InitSubmiter
{
    InitSubmiter(QString s,Submiter * in);
};

#endif // SUBMITER_H

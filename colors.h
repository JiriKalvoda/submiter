#ifndef COLORS_H
#define COLORS_H
#include<QString>

namespace color
{
        QString proces(int i=-1);
        const QString d = "\e[39m";
        const QString red = "\e[91m";
        const QString green = "\e[92m";
        const QString webPage = "\e[37m";
        const QString timer = "\e[94m";
        const QString uline = "\e[4m";
        const QString nouline = "\e[24m";
        QString replaceOkayError(QString in,QString def = d);
}

#endif // COLORS_H

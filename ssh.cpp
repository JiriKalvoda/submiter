#include "ssh.h"
#include "colors.h"
#include "login.h"
#include <QProcess>
#include <QTextStream>
#include <QRandomGenerator>
#include <QFileInfo>
#include <QTime>

extern QTextStream out;

int execute(QString s)
{
   out <<  color::uline << s << color::nouline << endl;
   return QProcess::execute(s);
}

void ssh(int argc,char ** argv)
{
    QString port = "22";
    if(argc < 3 || QString(argv[1]).mid(0,5)!="-ssh=") return;
    QString host = argv[1]+5;
    auto s = host.split(':');
    if(s.size()>=2)
    {
        host=s[0];
        port=s[1];
    }
    if(loginData.data.count("ssh"))
        host = loginData["ssh"].replace("#",host);
    QString dir = "/tmp/submiter_"+QString::number(QRandomGenerator(QDateTime::currentDateTime().toMSecsSinceEpoch()).generate64())+"/";
    try {
    if(execute("ssh "+host+" -p "+port+" \"mkdir "+dir+";chmod 700 "+dir+"\"" )) throw 1;
    int startArg=2;
    for(int i=startArg;i<argc;i++)
        if(argv[i][0]!='-')
        {
            QString fname = QString::number(i)+"-"+QFileInfo(argv[i]).fileName();
            if(execute(QString()+"scp -P "+port+" "+argv[i]+" "+host+":"+dir+fname)) throw 1;
        }
    QString c = "ssh "+host+" -p "+port+" submiter ";
    for(int i=startArg;i<argc;i++)
        if(argv[i][0]=='-')
            c+=" \""+QString(argv[i])+"\" ";
        else
        {
            QString fname = QString::number(i)+"-"+QFileInfo(argv[i]).fileName();
            c+= " "+dir+fname+"  -originalName=\""+QFileInfo(argv[i]).absoluteFilePath()+"\" ";
        }
    c+="";
    if(execute(c)) throw 1;
    if(execute("ssh "+host+" -p "+port+" \"rm "+dir+" -r\"" )) throw 1;
    }  catch (...) {
    if(execute("ssh "+host+" -p "+port+" \"rm "+dir+" -r\"" ))
    {
        fprintf(stderr,"Remove faild; pleas remove %s from remote mashine\n",dir.toStdString().c_str());
        exit(100);
    }

    }

}

#include "submiter.h"

#include<QProcess>
#include<QDir>
#include<QFileInfo>
#include <QElapsedTimer>

struct codeforces: public Submiter
{
    QString csrf;
    const QString server = "https://codeforces.com/";
    virtual QString detectTask(QString file)
    {
        auto d = file.split("/");
        return d.rbegin()[0].split(".")[0];
        fprintf(stderr,"Task not detect\n");
        exit(100);
    }
    virtual QString detectType(QString)
    {
        return "54";
    }
    QString parseInput(QString in,QString begin,QString end)
    {
        auto a = in.split(begin);
        if(a.size() < 2)
        {
            fprintf(stderr,"PARSE ERROR (%s,%s)\n",begin.toStdString().c_str(),end.toStdString().c_str());
            exit(100);
        }
        return a[1].split(end)[0];

    }
    virtual void logIn()
    {
        DOUT out << "----------------------------------------------------- LOGIN\n"<<endl;
        QString loginPage = wget(server+"enter");
        csrf = parseInput(loginPage,"<input type='hidden' name='csrf_token' value='","'");
        wget(server+"enter",{},{{"handleOrEmail",loginData["codeforces_login"]},{"passwd",loginData["codeforces_passwd"]},{"action","enter"},{"csrf_token",csrf}});

    }
    virtual void parseData(QString ansfer)
    {
        try {
            auto a = ansfer.split("<h3 class='task'>");
            if(a.length()<2) throw 1;
            auto b = a[0].split("</ul>");
            if(b.length()<3) throw 1;
            auto c = color::replaceOkayError(b.rbegin()[0],color::webPage);
            out << color::webPage << rmMulSpace(ignoreTag(c))  <<color::d<< endl<<endl;
        }  catch (...) {
            out << "CAN'T PARSE RESOULT" << endl << endl;
        }
    }
    virtual void submit(QString task, QString subtask, QString type, QByteArray data)
    {
        Q_UNUSED(type);
        DOUT out << "----------------------------------------------------- SUBMIT\n"<<endl;
        Parameters post = {{"csrf_token",csrf},{"action","submitSolutionFormSubmitted"},{"submittedProblemIndex",task},{"programTypeId",type},{"_tta","633"}};
        QString ansfer = wget(server+"contest/1352/problem/"+task+"?",{},post,{{"sourceFile",data,par["file_source"]}});
        parseData(ansfer);
        out << "URL: "<<server <<  task << endl;
    }
    virtual void logOut()
    {
        DOUT out << "----------------------------------------------------- LOGOUT\n"<<endl;
        //wget(server+"auth/logout.cgi",{},{{"redirect","https://ksp.mff.cuni.cz/"}});
    }
    codeforces()
    {
    }
    virtual ~codeforces()
    {
    }
};

InitSubmiter init_codeforces("codeforces",new codeforces);

#include <QCoreApplication>
#include <QString>
#include <QFileInfo>
#include <QDir>
#include <QElapsedTimer>

#include "submiter.h"
#include "ssh.h"

void parseArg(char * argv,QMap<QString,QString> & parM)
{
    int r=-1;
    for(int k=0;argv[k];k++)
        if(argv[k]=='=')
        {
            r=k;break;
        }
    if(r==-1)
        parM[argv]="TRUE";
    else if(!argv[r+1])
        parM[QString(argv).mid(0,r)] = "NULL";
    else
        parM[QString(argv).mid(0,r)] = argv+r+1;
}

QString detectCompetition(QString file)
{
    auto d = file.split("/");
    for(int i=d.size()-1;i>=0;i--)
    {
        if(submiter.count(d[i]))
            return d[i];
    }
    return "";
}
void run(QString file,QMap<QString,QString> parM)
{
    DARG
    {
        out << "FILE: " << file << endl;
        for(auto it=parM.begin();it!=parM.end();it++)
            out << "P "<< it.key() << ": "<<it.value()<<endl;
    }
    parM["file_source"] = file;
    if(parM["-originalName"]=="") parM["-originalName"]=QFileInfo(file).absoluteFilePath();
    if(parM["-c"]=="")
        parM["-c"] = detectCompetition(parM["-originalName"]);
    if(submiter.count(parM["-c"])==0)
    {
        fprintf(stderr,"COMPETITION %s NOT FOUND\n",parM["-c"].toStdString().c_str());
        exit(8);
    }
    submiter[parM["-c"]]->run(file,parM);


}
void noFileRun(QString file,QMap<QString,QString> parM)
{
    DARG
    {
        out << "NO FILE: " << file << endl;
        for(auto it=parM.begin();it!=parM.end();it++)
            out << "P "<< it.key() << ": "<<it.value()<<endl;
    }
    parM["file_source"] = file;
    if(parM["-originalName"]=="") parM["-originalName"]=QFileInfo(".").absoluteFilePath();
    if(parM["-c"]=="")
        parM["-c"] = detectCompetition(parM["-originalName"]);
    if(submiter.count(parM["-c"])==0)
    {
        fprintf(stderr,"COMPETITION %s NOT FOUND\n",parM["-c"].toStdString().c_str());
        exit(8);
    }
    submiter[parM["-c"]]->noFileRun(file,parM);
}
QTextStream out(stdout);
int main(int argc, char *argv[])
{
        QElapsedTimer timer;
        timer.start();
    if(argc==1)
    {
            fprintf(stderr,"Please select file.\nType -h for more informacion.\n");
            exit(100);
    }
    if(QString(argv[1]).mid(0,4)=="-ssh")
        ssh(argc,argv);
    else
    {
    for(int i=0;i<argc;i++)
    {
        if(QString(argv[i])=="-h")
        {
            out << globalHelp() << endl;
            return 0;
        }
    }
    for(int i=0;i<argc;i++)
    {
        if(QString(argv[i]).mid(0,3)=="-h=")
        {
            if(submiter.count(argv[i]+3))
                out << submiter[argv[i]+3]->help() << endl;
            else
                fprintf(stderr,"COMPETITION %s NOT FOUND\n",argv[i]+3);
            return 0;
        }
    }
    QMap<QString,QString> par;
    QCoreApplication a(argc, argv);
    //printf("%s\n",wget(url,{{"a","b"},{"c","d"}},{{"b","c","d"},{"fileToUpload","DATA AA"}}).toStdString().c_str());
    //printf("%s\n",wget(url,{{"a=b","b"},{"c","d"}}).toStdString().c_str());
    int i;
    for(i=1;argv[i][0]=='-' && argv[i][1]!='-';i++)
    {
        if(i+1 >= argc)
        {
            fprintf(stderr,"Please select file.\nType -h for more informacion.\n");
            exit(100);
        }
        parseArg(argv[i],par);
    }
    QString file=argv[i];
    QMap<QString,QString> parM=par;
    for(i++;i<argc;i++)
    {
        if(argv[i][0]=='-' && argv[i][1]!='-')
            parseArg(argv[i],parM);
        else
        {
            if(file[0]=='-')
                noFileRun(file,parM);
            else
                run(file,parM);
            file=argv[i];
            parM=par;
        }
    }
    if(file[0]=='-')
        noFileRun(file,parM);
    else
        run(file,parM);
    }
    for(auto it:submiter)
            delete it;
        out << color::timer << "TIME: "<<timer.elapsed()<< " ms"<< color::d<<endl;
    return 0;
}

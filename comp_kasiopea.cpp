#include "submiter.h"

#include<QProcess>
#include<QFile>
#include <QElapsedTimer>

struct kasiopea: public Submiter
{
    virtual QByteArray exec(QString name,QByteArray in)
    {
        if(par["-parallel"]=="") return Submiter::exec(name,in);
	if(name[0]!='/') name="./"+name;
        QElapsedTimer timer;
        timer.start();
        int len;
        sscanf(in.data(),"%d",&len);
        QProcess process[len];
        bool finish[len];
        out << "runnung "<<name<< " as " << len <<" proces"<< endl;
        for(int i=0;i<len;i++)
        {
            process[i].start(name,{QString::number(i)});
            process[i].setReadChannel(QProcess::StandardError);
            if (!process[i].waitForStarted())
                throw 2;
            process[i].write(in);
            process[i].closeWriteChannel();
            finish[i]=0;
        }
        char c[1234];
        int done=0;
        do
        {
            for(int i=0;i<len;i++)
            {
                if(finish[i]) continue;
                if(process[i].bytesAvailable() || !process[i].waitForFinished(1))
                {
                    if(process[i].bytesAvailable())
                    {
                        c[process[i].read(c,1234)]=0;
                        out <<color::proces(i) << c << color::d << flush;
                    }
                }
                else
                {
                    done++;
                    finish[i]=1;
                }
            }
        } while (done!=len);
        out << "end "<<name<<endl;
        QByteArray o;
        for(int i=0;i<len;i++)
            o += process[i].readAllStandardOutput();
        out << color::timer << "Running time: "<<timer.elapsed()<< " ms"<< color::d<<endl;
        return o;
    }
    const QString server = "https://kasiopea.matfyz.cz/";
    virtual QString detectTask(QString file)
    {
        auto d = file.split("/");
        if(d.size()>=2)
        {
		QString t = d.rbegin()[1];
		if('a'<=t[0]&&t[0]<='z') t[0]= t[0].row()+'A'-'a';
		if(t.size()==1 && (('A'<=t[0]&&t[0]<='Z')||('a'<=t[0]&&t[0]<='z')))
		    return "archiv/2019/doma/"+t;
	}
        fprintf(stderr,"Task not detect\n");
        exit(100);
    }
    virtual void logIn()
    {
        DOUT out << "----------------------------------------------------- LOGIN\n"<<endl;
        wget(server+"auth/login.cgi",{},{{"email",loginData["kasiopea_login"]},{"passwd",loginData["kasiopea_passwd"]},{"redirect",""},{"submit","Přihlásit"}});

    }
    virtual void parseData(QString ansfer)
    {
        try {
            auto a = ansfer.split("<h3 class='task'>");
            if(a.length()<2) throw 1;
            auto b = a[0].split("</ul>");
            if(b.length()<3) throw 1;
            auto c = color::replaceOkayError(b.rbegin()[0],color::webPage);
            out << color::webPage << rmMulSpace(ignoreTag(c))  <<color::d<< endl<<endl;
        }  catch (...) {
            out << "CAN'T PARSE RESOULT" << endl << endl;
        }
    }
    virtual void submit(QString task, QString subtask, QString type, QByteArray data)
    {
        Q_UNUSED(type);
        DOUT out << "----------------------------------------------------- SUBMIT\n"<<endl;
        Parameters post = {{"do","send"}};
        if(subtask!="NULL") post.push_back({"subtask",subtask});
        QString sourceFile = par["-code"]!=""?par["-code"]:par["file_source"];
        QFile f(sourceFile);
        f.open(QFile::ReadOnly);
        auto sourceData = f.readAll();
        f.close();
        QString ansfer = wget(server+task+"?",{},post,{{"f",data,"a"},{"s",sourceData,sourceFile}});
        parseData(ansfer);
        out << "URL: "<<server <<  task << endl;
    }
    virtual int countOfSubtask(QString task)
    {
        Q_UNUSED(task);
        return 2;
    }
    virtual void generate(QString task, QString subtask)
    {
        DOUT out << "----------------------------------------------------- GENERATE\n"<<endl;
        QString ansfer = wget(server+task,{{"do","gen"},{"subtask",subtask}});
        parseData(ansfer);
        out << "URL: "<<server <<  task << endl;
    }
        virtual QByteArray download(QString task, QString subtask)
    {
        DOUT out << "----------------------------------------------------- DOWNLOAD\n"<<endl;
        auto data = wget(server+task,{{"do","get"},{"subtask",subtask}}).toUtf8();
        return data;
    }
    virtual void logOut()
    {
        DOUT out << "----------------------------------------------------- LOGOUT\n"<<endl;
        wget(server+"auth/logout.cgi",{},{{"redirect","https://ksp.mff.cuni.cz/"}});
    }
    kasiopea()
    {
    }
    virtual ~kasiopea()
    {
    }
};

InitSubmiter init_kasiopea("kasiopea",new kasiopea);

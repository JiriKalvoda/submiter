#include "submiter.h"
#include "colors.h"

#include<QFileInfo>
#include<QDir>
#include <QElapsedTimer>

QString generateTable(QVector<QVector<QString>> in)
{
    QVector<int> len;
    QString out;
    for(auto & it: in)
    {
        for(int i=0;i<it.size();i++)
        {
            while(i>=len.size()) len.push_back(0);
            len[i]=qMax(len[i],it[i].size());
        }
    }
    for(int i=0;i<in.size();i++)
    {
        if(i==0||i==1)
        {
        for(int it : len)
        {
            out += "+";
            for(int k=0;k<it;k++) out+="-";
        }
        out+="+\n";
        }
        for(int j=0;j<in[i].size();j++)
        {
            out += "|" + in[i][j];
            for(int k=in[i][j].size();k<len[j];k++) out+=" ";
        }
        out+="|\n";
    }
        for(int it : len)
        {
            out += "+";
            for(int k=0;k<it;k++) out+="-";
        }
        out+="+\n";
    return out;

}

struct kspcz: public Submiter
{
    const QString server = "https://ksp.mff.cuni.cz/";
    const int year = 32;
    virtual QString pageName()
    {
	    if(par["-cviciste"]=="TRUE")
		    return "submit/cviciste.cgi";
	    return "submit/submit.cgi";
    }
    virtual QString detectTask(QString file)
    {
        auto d = file.split("/");
        if(d.size()>=3)
        {
            int t3 = d.rbegin()[1].toInt();
            int t2 = d.rbegin()[2].toInt();
            int t1 = d.size()>=3?d.rbegin()[3].toInt():0;

            if(!t1) t1 = year;
            if(t1 && t2 && t3)
                return QString::number(t1)+"-"+QString::number(t2)+"-"+QString::number(t3);
        }
        fprintf(stderr,"Task not detect\n");
        exit(100);
    }
    virtual QString detectType(QString fileName)
    {
           auto end = fileName.split(".").rbegin()[0];
           if(end=="pdf") return "PDF (Portable Document Format)";
           if(end=="zip") return "ZIP";
           return "";
    }
    virtual void logIn()
    {
        DOUT out << "----------------------------------------------------- LOGIN\n"<<endl;
        wget(server+"auth/login.cgi",{},{{"login",loginData["ksp_cz_login"]},{"passwd",loginData["ksp_cz_passwd"]},{"redirect","https://ksp.mff.cuni.cz/"},{"submit","Přihlásit"}});
    }
    virtual void parseData(QString ansfer)
    {
        try {
            auto a = ansfer.split("<h3>Testovací vstupy</h3>");
            if(a.length()<2) throw 1;
            auto c = a[1].split("</table>");
            if(c.length()<2) throw 1;
            out <<  color::webPage << rmMulSpace(ignoreTag(c[0])) <<color::d << endl<<endl;
        }  catch (...) {
        }
        try {
            auto a = ansfer.split("<h1 class='title'>");
            if(a.length()<2) throw 1;
            auto b = a[1].split("<h2>");
            if(b.length()<2) throw 1;
            auto c = b[1].split("</table>");
            if(c.length()<2) throw 1;
            out << color::webPage << rmMulSpace(ignoreTag(c[0])) << color::d << endl<<endl;
        }  catch (...) {
            out << "CAN'T PARSE RESOULT" << endl<<endl;
        }
        try {
            QString o;
            auto a = ansfer.split("<p class='");
            a.pop_front();
            for(QString it : a)
            {
                if(it.mid(0,6)=="okay'>")
                    o += color::green + it.mid(6).split("</p>")[0] + color::d;
                if(it.mid(0,7)=="error'>")
                    o += color::red + it.mid(7).split("</p>")[0] + color::d;
            }
            if(o == "") throw 1;
            out << "SERVER RESOULT: " << o << endl<<endl;
        }  catch (...) {
            out << "NO RESOULT" << endl << endl;
        }
    }
    virtual void submit(QString task, QString subtask, QString type, QByteArray data)
    {
        DOUT out << "----------------------------------------------------- SUBMIT\n"<<endl;
        Parameters post = {{"task",task}};
        if(type!="NULL") post.push_back({"t",type});
        if(subtask!="NULL") post.push_back({"sub",subtask});
        QString ansfer = wget(server+pageName()+"?",{},post,{{"f",data,"a"}});
        parseData(ansfer);
        out << "URL: "<<server<<pageName()+"?task="<<  task << endl;
    }
    virtual int countOfSubtask(QString task)
    {
        DOUT out << "----------------------------------------------------- COUNT OF SUBTASK\n"<<endl;
        Parameters post = {{"task",task}};
        QString ansfer = wget(server+pageName()+"?",{},post);
        parseData(ansfer);
        out << "URL: "<<server<<pageName()+"?task="<<  task << endl;
        int size = ansfer.split("<form action='?' method='post'><input name='task' type='hidden' value").size()-1;
        size += ansfer.split("<form action='?' method='post'><input name='year' type='hidden' value").size()-1;
        out << "COUNT OF SUBMIT: " << size << endl << endl;
        return size;
    }
    virtual void generate(QString task,QString subtask)
    {
        DOUT out << "----------------------------------------------------- GENERATE\n"<<endl;
        Parameters post = {{"task",task},{"gen","Generuj"}};
        if(subtask!="NULL") post.push_back({"sub",subtask});
        QString ansfer = wget(server+pageName()+"?",{},post);
        parseData(ansfer);
        out << "URL: "<<server<<pageName()+"?task="<<  task << endl;
    }
    virtual QByteArray download(QString task, QString subtask)
    {
        DOUT out << "----------------------------------------------------- DOWNLOAD\n"<<endl;
        Parameters post = {{"task",task},{"in","1"}};
        if(subtask!="NULL") post.push_back({"sub",subtask});
        return wget(server+pageName()+"?",{},post).toUtf8();
    }
        virtual void logOut()
    {
        DOUT out << "----------------------------------------------------- LOGOUT\n"<<endl;
        wget(server+"auth/logout.cgi",{},{{"redirect","https://ksp.mff.cuni.cz/"}});
    }
    struct User
    {
        QString uid;
        QString name;
        QMap<QString,QString> task;
        QMap<QString,QVector<double> > taskScore;
        double skore = 0;
        double bodu(QString in)
        {
            auto a = in.split("→ ")[1].split("(")[0].trimmed();
            auto b = in.split("(z")[1].split(")")[0].trimmed();
            if(a=="?") return b.toDouble();
            return a.toDouble();
        }
        void load()
        {
            QString in = wget("https://ksp.mff.cuni.cz/profil/profil.cgi?uid="+uid);
            name = in.split("<span class=\"name\">")[1].split("</span>")[0];
            auto s = in.split("\">32-");
            for(int i=1;i<s.size();i++)
            {
                auto a = s[i].split(":")[0];
                auto b = ignoreTag(s[i].split("<td>")[1].split("</td>")[0]);
                if(a[0]!='Z')
                {
                    task[a]=b;
                    taskScore[a.split("-")[0]].push_back(bodu(b));
                }
            }
            for(auto & it : taskScore)
            {

                std::sort(it.begin(),it.end(),[](double a,double b)->bool{return b<a;});
                for(int i=0;i<qMin(5,it.size());i++)
                {
                    //out << it[i] << " ";
                    skore += it[i];
                }
                //out << endl;
            }
            //out<<endl;
        }
	User(){}
        User(QString UID):uid(UID)
        {
            load();
        }
    };

    virtual void noFileRun(QString fileName,QMap<QString,QString> parM)
    {
        if(fileName=="--score")
        {
            QString in = wget("https://ksp.mff.cuni.cz/tasks/32/score3.html");
            auto s =in.split("/profil/profil.cgi?uid=");
            QVector<User> user;
            for(int i=1;i<qMin(s.size(),parM.count("-limit")?parM["-limit"].toInt()+1:1000);i++)
                user.push_back(User(s[i].split("\"")[0]));
            QMap<QString,QString> allTask;
            for(auto & it : user)
            {
                for(auto i=it.task.begin();i!=it.task.end();i++)
                {
                    if(!parM.count("-serie") || i.key()[0]==parM["-serie"])
                        allTask[i.key()]=i.key();
                }
            }
            std::sort(user.begin(),user.end(),[](const User & a,const User & b)->bool{return a.skore>=b.skore;});
            QVector<QVector<QString>> table;
            table.push_back(QVector<QString>());
            table.last().push_back("NAME");
            for(auto it:allTask)
                table.last().push_back(it);
            table.last().push_back("TOTAL");
            for(auto u:user)
            {
                table.push_back(QVector<QString>());
                table.last().push_back(u.name);
                for(auto it:allTask)
                    table.last().push_back(u.task.count(it)?u.task[it].split("(")[0].trimmed():"");
                table.last().push_back(QString::number(u.skore,'q',2));
            }
            out << generateTable(table);

        }
    }
    kspcz()
    {
    }
    virtual ~kspcz()
    {
    }
};

InitSubmiter init_ksp_cz("ksp_cz",new kspcz);

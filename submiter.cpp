#include "submiter.h"

#include <QCoreApplication>
#include<bits/stdc++.h>
#include <QFile>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QTimer>
#include <QString>
#include <QProcess>
#include <QFileInfo>
#include <QDir>
#include <QStringList>
#include <QElapsedTimer>
QMap<QString, Submiter *> submiter;

QNetworkAccessManager manager;
QString rmMulSpace(QString in)
{
    QString out="";
    auto s = in.split("\n");
    for(auto & it : s) it = it.trimmed();
    bool lastSpace = 0;
    bool addNl=0;
    for(auto & it : s)
    {
        if(it=="") continue;
        if(addNl)
            out += "\n";
        for(auto c : it)
        {
            if(c==' ')
            {
                if(lastSpace==0) out += ' ';
                lastSpace=1;
            }
            else
            {
                out+=c;
                lastSpace=0;
            }
        }
        addNl=1;
    }
    return out;
}
QString ignoreTag(QString in)
{
    QString out="";
    bool inTag=0;
    for(auto c:in)
    {
        if(c=='<') inTag=1;
        else
            if(c=='>')
            {
                inTag=0;
                out += ' ';
            }
        else
                if(!inTag) out += c;
    }
    return out;
}
    QByteArray buildUploadString(Parameters post,FileParameters files)
{
    QString bound="margin";
    QByteArray data;
    for(auto it : post)
    {
        data.append(QString("--" + bound + "\r\n").toUtf8());
        data.append("Content-Disposition: form-data; name=\"");
        data.append(it.first);
        data.append("\"\r\n\r\n");
        data.append(it.second);
        data.append("\r\n");
    }
    for(auto it: files)
    {
        data.append(QString("--" + bound + "\r\n").toUtf8());
        data.append("Content-Disposition: form-data; name=\"");
        data.append(it.name);
        data.append("\"; filename=\"");
        data.append(it.filename);
        data.append("\"\r\n");
        data.append("Content-Type: text/xml\r\n\r\n"); //data type

        data.append(it.in);
        data.append("\r\n");
    }
    data.append("--" + bound + "--\r\n");  //closing boundary according to rfc 1867

    return data;
}

QString wget(QString url,Parameters get,Parameters post,FileParameters files)
{

    if(get.size())
    {
        bool hasQuestion=0;
        for(auto it:url)
        {
            if(it=='?') hasQuestion=1;
        }
        for(auto it:get)
        {
            url += hasQuestion?"&":"?";
            hasQuestion=1;
            url += QUrl::toPercentEncoding(it.first);
            url += "=";
            url += QUrl::toPercentEncoding(it.second);
        }

    }

    QByteArray data;
    QNetworkRequest request(url);
    //        request.setAttribute(QNetworkRequest::FollowRedirectsAttribute,1);
    //        request.setAttribute(QNetworkRequest::RedirectPolicyAttribute,QNetworkRequest::NoLessSafeRedirectPolicy);
    QNetworkReply * reply;
    request.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/x-www-form-urlencoded"));
    QByteArray postData = buildUploadString(post,files);
    DASK printf("---\n%s:\n%s\n---\n",url.toStdString().c_str(),postData.data());
    if(post.size() || files.size())
    {
        QString bound="margin";
        request.setRawHeader(QString("Content-Type").toUtf8(),QString("multipart/form-data; boundary=" + bound).toUtf8());
        request.setRawHeader(QString("Content-Length").toUtf8(), QString::number(postData.length()).toUtf8());
        reply= manager.post(request,postData);
    }
    else
        reply= manager.get(request);
    QEventLoop loop;
    //----------------------------------------------
    QTimer timer;
    timer.setSingleShot(true);
    QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    timer.start(60*1000);   // 30 secs. timeout
    loop.exec();

    if(!timer.isActive())
    {
        // timeout
        QObject::disconnect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        reply->abort();
        fprintf(stderr,"********** TIMEOUT **********\n");
        exit(1);

    }
    //----------------------------------------------
    data = reply->readAll();
    DOUT out << data<<endl;
    QUrl redirect=reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    while(redirect.isValid())
    {
        DASK out << "********** REDIRECT **********" << redirect.toString() << endl;
        QNetworkRequest request(redirect);
        reply= manager.get(request);
        QEventLoop loop;
        QTimer timer;
        timer.setSingleShot(true);
        QObject::connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
        QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
        timer.start(10*1000);   // 30 secs. timeout
        loop.exec();
        if(!timer.isActive())
        {
            // timeout
            QObject::disconnect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
            reply->abort();
            fprintf(stderr,"********** TIMEOUT **********\n");
            exit(1);

        }
        data = reply->readAll();
        DOUT out << data<<endl;
        redirect=reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
    }
    if(reply->error())
    {
        fprintf(stderr,"********** ERROR ********** %s\n",reply->errorString().toStdString().c_str());
        exit(1);
    }
    return data;
}

    QByteArray Submiter::exec(QString name,QByteArray in)
    {
	if(name[0]!='/') name="./"+name;
        QElapsedTimer timer;
        timer.start();
        QProcess process;
        out << "runnung "<<name<<endl;
        process.start(name, QStringList());
        //process.setStandardErrorFile("/dev/stdout");
        process.setReadChannel(QProcess::StandardError);
        if (!process.waitForStarted())
            throw 2;

        process.write(in);
        process.closeWriteChannel();
        char c[1234];
        while(process.bytesAvailable() || !process.waitForFinished(1))
            while(process.bytesAvailable())
            {
                c[process.read(c,1234)]=0;
                out <<color::proces() << c << color::d << flush;
            }
        out << "end "<<name<<endl;
        auto r =  process.readAllStandardOutput();
        out << color::timer << "Running time: "<<timer.elapsed()<< " ms"<< color::d<<endl;
        return r;
    }
    QString Submiter::makeExec(QString in)
    {
        QElapsedTimer timer;
        timer.start();
        auto end = in.split(".").rbegin()[0];
        auto withoutEnd = in.mid(0,in.size()-end.size()-1);

        if(end=="cpp")
        {
            if(QProcess::execute("g++ -Wall "+in+" -o "+withoutEnd+".out -O2"))
            {
                fprintf(stderr,"COMPILATION ERROR\n");
                exit(100);
            }
        out << color::timer << "C++ Compilation time: "<<timer.elapsed()<< " ms"<< color::d<<endl;
            return withoutEnd+".out";
        }
        return in;
    }
    QMap<QString,QString> par;
    QString Submiter::detectTask(QString file)
    {
        auto d = QFileInfo(file).absoluteDir();
        int t3 = d.dirName().toInt();
        d.cdUp();
        int t2 = d.dirName().toInt();
        if(t2 && t3)
            return QString::number(t2)+"-"+QString::number(t3);
        fprintf(stderr,"Task not detect\n");
        exit(100);
    }
    QString Submiter::detectType(QString fileName)
    {
        auto end = fileName.split(".").rbegin()[0];
        if(end=="pdf") return "PDF (Portable Document Format)";
        if(end=="zip") return "ZIP";
        return "";
    }
    void Submiter::logIn()
    {
    }
    void Submiter::parseData(QString ansfer)
    {
        Q_UNUSED(ansfer);
    }
    void Submiter::submit(QString task, QString subtask, QString type, QByteArray data)
    {
        Q_UNUSED(task);
        Q_UNUSED(subtask);
        Q_UNUSED(type);
        Q_UNUSED(data);
    }
    void Submiter::submitAll(QString task, QString subtask, QString type, QByteArray data)
    {
        int size = data.size();
        QElapsedTimer timer;
        timer.start();
        submit(task,subtask,type,data);
        out << color::timer << "Submit time: "<<timer.elapsed()<<" ms; size "<<size/1024<<"  kb"<< color::d<<endl;
    }
    int Submiter::countOfSubtask(QString task)
    {
        Q_UNUSED(task);
        return 1;
    }
    QByteArray Submiter::generateDownloadAll(QString task, QString subtask)
    {
        QElapsedTimer timer;
        timer.start();
        generate(task,subtask);
        out << color::timer << "Generate time: "<<timer.elapsed()<< " ms"<< color::d<<endl;
        QElapsedTimer timer2;
        timer2.start();
        auto d = download(task,subtask);
        out << color::timer << "Download time: "<<timer2.elapsed()<< " ms; size "<<d.size()/1024<<" kb"<< color::d<<endl;
        return d;
    }
    QByteArray Submiter::download(QString task, QString subtask)
    {
        Q_UNUSED(task);
        Q_UNUSED(subtask);
        fprintf(stderr,"DOWNLOAD IS NOT POSSIBLE FOR THIS COMPETITION\n");
        exit(50);
        return QByteArray();
    }
    void Submiter::generate(QString task, QString subtask)
    {
        Q_UNUSED(task);
        Q_UNUSED(subtask);
    }
    void Submiter::logOut()
    {
    }
    void Submiter::run(QString fileName,QMap<QString,QString> parM)
    {
        par=parM;
        if(!isLogIn) logIn();
        isLogIn = 1;
        QString task = parM["-t"]!=""?parM["-t"]:detectTask(parM["-originalName"]);
        QString type=parM["-type"]!=""?parM["-type"]:detectType(parM["-originalName"]);
        QString subtask="NULL";
        if(parM["-s"]!="")
            subtask=parM["-s"];
        if(parM["-r"]!="")
        {
            fileName = makeExec(fileName);
            if(subtask=="NULL")
            {
                int lenSubtask = countOfSubtask(task);
                for(int i=1;i<=lenSubtask;i++)
                {
                    out << endl;
                    out << "*************" <<"*"<< "****" << endl;
                    out << "*** SUBTASK: " << i << " ***" << endl;
                    out << "*************" <<"*"<< "****" << endl<<endl;
                    auto input = generateDownloadAll(task,QString::number(i));
                    auto output = exec(fileName,input);
                    out << endl;
                    submitAll(task,QString::number(i),type,output);
                }
            }
            else
            {
                auto input = generateDownloadAll(task,subtask);
                auto output = exec(fileName,input);
                out << endl;
                submitAll(task,subtask,type,output);
            }
        }
        else
            if(parM["-d"]!="")
            {
                QFile file (fileName);
                auto data = generateDownloadAll(task,subtask);
                if(!file.open(QFile::WriteOnly))

                {
                    fprintf(stderr,"CAN'T OPEN FILE!\n");
                    logOut();
                    exit(2);
                }
                file.write(data);
                file.close();
            }
            else
            {
                QFile file (fileName);
                if(!file.open(QFile::ReadOnly))
                {
                    fprintf(stderr,"CAN'T OPEN FILE!\n");
                    logOut();
                    exit(2);
                }
                auto data = file.readAll();
                file.close();
                submitAll(task,subtask,type,data);
            }
    }
    Submiter::~Submiter()
    {
        if(isLogIn) logOut();
        isLogIn=0;
    }
    QString Submiter::help()
    {
        return "No help available for this competition\n";
    }

QString submiterJoin(QString in)
{
    QString o;
    for(auto it = submiter.begin();it!=submiter.end();it++)
        o +=  in + it.key();
    return o.mid(in.length());
}
QString globalHelp()
{
QString out =
    "This is SUBMITER - program for submit solution of  competition\n"
    "\n"
    "Usage:  submiter [ssh option] [global option] FILE1 [FILE1 option] { FILE2 [FILE2 option] { ...} }\n"
    "\n"
    "ssh option:   -ssh=ADDRES   Use server at ADDRES.\n"
    "                            Only for submit or run (no download).\n"
    "                            Submiter with login data must be install there.\n"
    "\n"
    "option:  -c=COMP      set competition to COMP (automatical by path)\n"
    "                        {" + submiterJoin(";")+"} \n"
    "         -t=TASK      set task name to TASK (automatical by path or file name)\n"
    "         -s=SUBTASK   set subtask name to SUBTASK (automatical all)\n"
    "         -type=TYPE   set file type to TYPE (automatical by extension)\n"
    "         -d           download input file (only some competition)\n"
    "         -r           download, run and submit (only some competition)\n"
    "         -originalName=ABSOLUTEADRES    use this path for determination task, type and competition ...\n"
    "         -h           print this help";
return out;
}

InitSubmiter::InitSubmiter(QString s,Submiter * in)
{
    if(submiter.count(s)) assert(0);
    submiter[s]=in;
}

void Submiter::noFileRun(QString fileName,QMap<QString,QString> parM)
{
    Q_UNUSED(fileName);
    Q_UNUSED(parM);

}
